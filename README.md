# README #

This is techinal challenge for Class Super interview. This exercise create a simple customer transactions application.

# Note #
1. Solution is set to target .net framework 4.6.1.
1. Application is written using WPF by following MVVM design pattern, there is strictly no code-behind in the code.
1. Application will read Customers.csv and Transactions.csv in Resources/ directory (These files are provided in the instructions).

# How-to #
1. Main window consists of two data grids, one is for listing customers and the others is for listing transactions for selected customer.
1. Starting the application will load customers and transactions data, list of customers will be displayed on Customers data grid.
1. Right click on Customers data grid for context menu to filter customers with multiple accounts and without any account.
1. Select specific customer will populate transactions for selected customer on the Transactions data grid.
1. Right click on Transactions data grid for context menu to identify these transactions:
    - Transactions made between 18.00 and 06.00 (highlighted in light blue colour).
    - Cash out transactions with amount more than half of account balance right before transaction's been placed. (hightlighted in light salmon colour).

# Improvent #
1. Ability to load input Customers.csv and Transactions.csv.
1. Business logic should be separated from view model.

