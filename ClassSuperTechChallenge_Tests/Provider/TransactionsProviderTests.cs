﻿using System;
using NUnit.Framework;
using ClassSuperTechChallenge.Provider;
using System.IO;
using ClassSuperTechChallenge.Model;

namespace ClassSuperTechChallenge_Tests.Provider
{
    [TestFixture]
    class TransactionsProviderTests
    {
        private TransactionsProvider mTestObject;

        [SetUp]
        public void Setup()
        {
            mTestObject = new TransactionsProvider();
        }

        [Test]
        public void Test_CreateInstance_Successful_TransactionsAreEmpty()
        {
            Assert.NotNull(mTestObject);
            CollectionAssert.IsEmpty(mTestObject.Transactions);
        }

        [TestCase(null, typeof(ArgumentNullException))]
        [TestCase("", typeof(ArgumentNullException))]
        [TestCase("InvalidPath", typeof(FileNotFoundException))]
        public void Test_Load_InvalidPath_ExceptionThrown(string path, Type expectedException)
        {
            Assert.Throws(expectedException, () => mTestObject.Load(path));
        }

        [Test]
        public void Test_Load_Succesful_TransactionsArePopulated()
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Provider/TransactionsTestData.csv");

            mTestObject.Load(path);

            Assert.AreEqual(11, mTestObject.Transactions.Count);
            Assert.AreEqual(500, mTestObject.Transactions[0].Amount, "Check first transaction");
            Assert.AreEqual(TransactionCode.CIN, mTestObject.Transactions[0].TransactionCode, "Check first transaction");
            Assert.AreEqual(55, mTestObject.Transactions[10].Amount, "Check last transaction");
            Assert.AreEqual(TransactionCode.COT, mTestObject.Transactions[10].TransactionCode, "Check last transaction");
        }

        [TestCase(1, 7)]
        [TestCase(2, 4)]
        [TestCase(3, 0)]
        public void Test_GetTransactions_Successful_CorrectTransactionsAreReturned(int customerId, int expectedTransactionsCount)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Provider/TransactionsTestData.csv");
            mTestObject.Load(path);

            var result = mTestObject.GetTransactions((uint)customerId);

            Assert.AreEqual(expectedTransactionsCount, result.Count);
        }
    }
}
