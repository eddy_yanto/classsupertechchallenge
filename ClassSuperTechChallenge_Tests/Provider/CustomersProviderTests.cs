﻿using System;
using NUnit.Framework;
using ClassSuperTechChallenge.Provider;
using System.IO;

namespace ClassSuperTechChallenge_Tests.Provider
{
    [TestFixture]
    class CustomersProviderTests
    {
        private CustomersProvider mTestObject;

        [SetUp]
        public void Setup()
        {
            mTestObject = new CustomersProvider();
        }

        [Test]
        public void Test_CreateInstance_Successful_CustomersAreEmpty()
        {
            Assert.NotNull(mTestObject);
            CollectionAssert.IsEmpty(mTestObject.Customers);
        }

        [TestCase(null, typeof(ArgumentNullException))]
        [TestCase("", typeof(ArgumentNullException))]
        [TestCase("InvalidPath", typeof(FileNotFoundException))]
        public void Test_Load_InvalidPath_ExceptionThrown(string path, Type expectedException)
        {
            Assert.Throws(expectedException, () => mTestObject.Load(path));
        }

        [Test]
        public void Test_Load_Succesful_CustomersArePopulated()
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Provider/CustomersTestData.csv");

            mTestObject.Load(path);

            Assert.AreEqual(3, mTestObject.Customers.Count);
            for (int n = 0; n < mTestObject.Customers.Count; n++)
            {
                Assert.AreEqual(n + 1, mTestObject.Customers[n].Id);
            }
        }
    }
}
