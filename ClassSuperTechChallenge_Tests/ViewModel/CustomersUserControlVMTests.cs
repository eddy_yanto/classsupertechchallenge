﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.ViewModel;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassSuperTechChallenge_Tests.ViewModel
{
    class CustomersUserControlVMTests
    {
        public Mock<ICustomersProvider> mCustomersProviderMock;
        public Mock<ITransactionsProvider> mTransactionsProviderMock;

        [SetUp]
        public void Setup()
        {
            mCustomersProviderMock = new Mock<ICustomersProvider>();
            mTransactionsProviderMock = new Mock<ITransactionsProvider>();

            var customers = new List<ICustomer>()
            {
                new Customer(1, "Customer", "1"),
                new Customer(2, "Customer", "2"),
                new Customer(3, "Customer", "3")
            };
            mCustomersProviderMock.SetupGet(provider => provider.Customers).Returns(customers);

            var customer1transactions = new List<ITransaction>()
            {
                new Transaction(1, 1, 100, TransactionCode.CIN, 100, new DateTime()),
                new Transaction(1, 2, 100, TransactionCode.CIN, 100, new DateTime()),
                new Transaction(1, 1, 50, TransactionCode.COT, 50, new DateTime()),
                new Transaction(1, 2, 75, TransactionCode.COT, 25, new DateTime()),
            };
            var customer2transactions = new List<ITransaction>()
            {
                new Transaction(2, 1, 200, TransactionCode.CIN, 200, new DateTime()),
            };
            var transactions = new List<ITransaction>(customer1transactions);
            transactions.AddRange(customer2transactions);
            mTransactionsProviderMock.Setup(provider => provider.GetTransactions(1)).Returns(customer1transactions);
            mTransactionsProviderMock.Setup(provider => provider.GetTransactions(2)).Returns(customer2transactions);
            mTransactionsProviderMock.Setup(provider => provider.GetTransactions(3)).Returns(new List<ITransaction>());
            mTransactionsProviderMock.SetupGet(provider => provider.Transactions).Returns(transactions);
        }

        [Test]
        public void Test_LoadCustomers_Successful_AllCustomersArePopulated()
        {
            var testObject = new CustomersUserControlVM(mCustomersProviderMock.Object, mTransactionsProviderMock.Object);

            testObject.LoadCustomersCommand.Execute(null);

            Assert.AreEqual(mCustomersProviderMock.Object.Customers.Count, testObject.Customers.Count);
            for (int n = 0; n < mCustomersProviderMock.Object.Customers.Count; n++)
            {
                Assert.AreEqual(mCustomersProviderMock.Object.Customers[n].Id, testObject.Customers[n].CustomerId);
                Assert.AreEqual(mCustomersProviderMock.Object.Customers[n].FirstName, testObject.Customers[n].FirstName);
                Assert.AreEqual(mCustomersProviderMock.Object.Customers[n].LastName, testObject.Customers[n].LastName);
            }
        }

        [Test]
        public void Test_ShowAllCustomers_Successful_FilterIsNull()
        {
            var testObject = new CustomersUserControlVM(mCustomersProviderMock.Object, mTransactionsProviderMock.Object);
            testObject.LoadCustomersCommand.Execute(null);

            testObject.ShowAllCustomersCommand.Execute(null);

            Assert.Null(testObject.CustomersView.Filter);
        }

        [Test]
        public void Test_ShowCustomersWithMultipleAccount_Successful_CorrectCustomersAreShown()
        {
            var testObject = new CustomersUserControlVM(mCustomersProviderMock.Object, mTransactionsProviderMock.Object);
            testObject.LoadCustomersCommand.Execute(null);

            testObject.ShowCustomersWithMultipleAccountCommand.Execute(null);

            var result = testObject.Customers.Where(c => testObject.CustomersView.Filter.Invoke(c)).ToList();
            Assert.NotNull(testObject.CustomersView.Filter);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].CustomerId);
        }

        [Test]
        public void Test_ShowCustomersWithoutAccount_Successful_CorrectCustomersAreShown()
        {
            var testObject = new CustomersUserControlVM(mCustomersProviderMock.Object, mTransactionsProviderMock.Object);
            testObject.LoadCustomersCommand.Execute(null);

            testObject.ShowCustomersWithoutAccountCommand.Execute(null);

            var result = testObject.Customers.Where(c => testObject.CustomersView.Filter.Invoke(c)).ToList();
            Assert.NotNull(testObject.CustomersView.Filter);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(3, result[0].CustomerId);
        }
    }
}
