﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.ViewModel;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ClassSuperTechChallenge_Tests.ViewModel
{
    class CustomerRowVMTests
    {
        public Mock<ITransactionsProvider> mTransactionsProviderMock;

        [SetUp]
        public void Setup()
        {
            mTransactionsProviderMock = new Mock<ITransactionsProvider>();
        }

        [Test]
        public void Test_CreateInstance_Successful_AllPropertiesAreCorrect()
        {
            var customer = new Customer(1, "First", "Last");
            var transactions = new List<ITransaction>()
            {
                new Transaction(1, 1, 100, TransactionCode.CIN, 100, new DateTime()),
                new Transaction(1, 2, 100, TransactionCode.CIN, 100, new DateTime()),
                new Transaction(1, 1, 50, TransactionCode.COT, 50, new DateTime()),
                new Transaction(1, 2, 75, TransactionCode.COT, 25, new DateTime()),
            };
            mTransactionsProviderMock.Setup(provider => provider.GetTransactions(customer.Id)).Returns(transactions);

            var testObject = new CustomerRowVM(customer, mTransactionsProviderMock.Object);

            Assert.AreEqual(customer.Id, testObject.CustomerId);
            Assert.AreEqual(customer.FirstName, testObject.FirstName);
            Assert.AreEqual(customer.LastName, testObject.LastName);
            Assert.AreEqual(2, testObject.NoOfAccount);
            Assert.AreEqual(75, testObject.Balance);
        }
    }
}
