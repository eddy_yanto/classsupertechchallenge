﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.ViewModel;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ClassSuperTechChallenge_Tests.ViewModel
{
    class TransactionsUserControlVMTest
    {
        public Mock<ITransactionsProvider> mTransactionsProviderMock;

        [SetUp]
        public void Setup()
        {
            mTransactionsProviderMock = new Mock<ITransactionsProvider>();

            var transactions = new List<ITransaction>()
            {
                // Normal hours transaction
                new Transaction(1, 1, 100, TransactionCode.CIN, 100, new DateTime(2017, 1, 1, 9, 0, 0)),
                // After hours transaction
                new Transaction(1, 2, 100, TransactionCode.CIN, 100, new DateTime(2017, 1, 1, 20, 0, 0)),
                // Cash out not more than half of balance transaction
                new Transaction(1, 1, 50, TransactionCode.COT, 50, new DateTime(2017, 1, 1, 9, 0, 0)),
                // Cash out more than half of balance transaction
                new Transaction(1, 2, 75, TransactionCode.COT, 25, new DateTime(2017, 1, 1, 9, 0, 0)),
            };
            mTransactionsProviderMock.Setup(provider => provider.GetTransactions(1)).Returns(transactions);
            mTransactionsProviderMock.SetupGet(provider => provider.Transactions).Returns(transactions);
        }

        [Test]
        public void Test_LoadTransactions_Successful_CorrectTransactionsAreShown()
        {
            var customer = new CustomerRowVM(new Customer(1, "First", "Last"), mTransactionsProviderMock.Object);
            var testObject = new TransactionsUserControlVM(mTransactionsProviderMock.Object);

            testObject.LoadTransactions(customer);

            Assert.AreEqual(mTransactionsProviderMock.Object.Transactions.Count, testObject.Transactions.Count);
            for (int n = 0; n < mTransactionsProviderMock.Object.Transactions.Count; n++)
            {
                Assert.AreEqual(mTransactionsProviderMock.Object.Transactions[n].AccountId, testObject.Transactions[n].AccountId);
                Assert.AreEqual(mTransactionsProviderMock.Object.Transactions[n].TransactionCode, testObject.Transactions[n].TransactionCode);
                Assert.AreEqual(mTransactionsProviderMock.Object.Transactions[n].Amount, testObject.Transactions[n].Amount);
                Assert.AreEqual(mTransactionsProviderMock.Object.Transactions[n].Balance, testObject.Transactions[n].Balance);
                Assert.AreEqual(mTransactionsProviderMock.Object.Transactions[n].Date, testObject.Transactions[n].Date);
            }
        }

        [Test]
        public void Test_ResetConditions_Successful_AllConditionsAreReset()
        {
            var customer = new CustomerRowVM(new Customer(1, "First", "Last"), mTransactionsProviderMock.Object);
            var testObject = new TransactionsUserControlVM(mTransactionsProviderMock.Object);
            testObject.LoadTransactions(customer);
            for (int n = 0; n < mTransactionsProviderMock.Object.Transactions.Count; n++)
            {
                testObject.Transactions[n].SetAfterHoursCondition();
                testObject.Transactions[n].SetCashOutMoreThanHalfOfBalanceCondition();
            }

            testObject.ResetConditionsCommand.Execute(null);

            for (int n = 0; n < mTransactionsProviderMock.Object.Transactions.Count; n++)
            {
                Assert.False(testObject.Transactions[n].IsAfterHoursTransaction);
                Assert.False(testObject.Transactions[n].IsCashOutMoreThanHalfOfBalance);
            }
        }

        [Test]
        public void Test_IdentifyAfterHoursTransactions_Successful_CorrectConditionsAreSet()
        {
            var customer = new CustomerRowVM(new Customer(1, "First", "Last"), mTransactionsProviderMock.Object);
            var testObject = new TransactionsUserControlVM(mTransactionsProviderMock.Object);
            testObject.LoadTransactions(customer);

            testObject.IdentifyAfterHoursTransactionsCommand.Execute(null);

            Assert.False(testObject.Transactions[0].IsAfterHoursTransaction);
            Assert.True(testObject.Transactions[1].IsAfterHoursTransaction);
            Assert.False(testObject.Transactions[2].IsAfterHoursTransaction);
            Assert.False(testObject.Transactions[3].IsAfterHoursTransaction);
        }

        [Test]
        public void Test_IdentifyCashOutMoreThanHalfOfBalanceTransactions_Successful_CorrectConditionsAreSet()
        {
            var customer = new CustomerRowVM(new Customer(1, "First", "Last"), mTransactionsProviderMock.Object);
            var testObject = new TransactionsUserControlVM(mTransactionsProviderMock.Object);
            testObject.LoadTransactions(customer);

            testObject.IdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand.Execute(null);

            Assert.False(testObject.Transactions[0].IsCashOutMoreThanHalfOfBalance);
            Assert.False(testObject.Transactions[1].IsCashOutMoreThanHalfOfBalance);
            Assert.False(testObject.Transactions[2].IsCashOutMoreThanHalfOfBalance);
            Assert.True(testObject.Transactions[3].IsCashOutMoreThanHalfOfBalance);
        }
    }
}
