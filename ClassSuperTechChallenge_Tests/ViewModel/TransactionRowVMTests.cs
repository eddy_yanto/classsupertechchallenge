﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.ViewModel;
using Moq;
using NUnit.Framework;
using System;

namespace ClassSuperTechChallenge_Tests.ViewModel
{
    class TransactionRowVMTests
    {
        public Mock<ITransaction> mTransactionMock;

        [SetUp]
        public void Setup()
        {
            mTransactionMock = new Mock<ITransaction>();
        }

        [Test]
        public void Test_CreateInstance_Successful_AllPropertiesAreCorrect()
        {
            var transaction = new Transaction(1, 1, 100, TransactionCode.CIN, 100, DateTime.Now);

            var testObject = new TransactionRowVM(transaction);

            Assert.AreEqual(transaction.AccountId, testObject.AccountId);
            Assert.AreEqual(transaction.TransactionCode, testObject.TransactionCode);
            Assert.AreEqual(transaction.Balance, testObject.Balance);
            Assert.AreEqual(transaction.Amount, testObject.Amount);
            Assert.AreEqual(transaction.Date, testObject.Date);
        }

        [TestCase(17, 59, 59, false)]
        [TestCase(18, 0, 0, true)]
        [TestCase(18, 0, 1, true)]
        [TestCase(05, 59, 59, true)]
        [TestCase(06, 0, 0, true)]
        [TestCase(06, 0, 1, false)]
        public void Test_IsAfterHoursTransaction_DifferentHours_FlagIsSetCorrectly(int hour, int minute, int second, bool expectedResult)
        {
            var transaction = new Transaction(1, 1, 100, TransactionCode.CIN, 100, new DateTime(2017, 1, 1, hour, minute, second));
            var testObject = new TransactionRowVM(transaction);

            testObject.SetAfterHoursCondition();

            Assert.AreEqual(expectedResult, testObject.IsAfterHoursTransaction);
        }

        [TestCase(TransactionCode.COT, 49, 50, false)]
        [TestCase(TransactionCode.COT, 50, 50, false)]
        [TestCase(TransactionCode.COT, 51, 50, true)]
        [TestCase(TransactionCode.CIN, 49, 50, false)]
        [TestCase(TransactionCode.CIN, 50, 50, false)]
        [TestCase(TransactionCode.CIN, 51, 50, false)]
        public void Test_IsCashOutMoreThanHalfOfBalance_DifferentConditions_FlagIsSetCorrectly(TransactionCode code, decimal amount, decimal balance, bool expectedResult)
        {
            var transaction = new Transaction(1, 1, amount, code, balance, DateTime.Now);
            var testObject = new TransactionRowVM(transaction);

            testObject.SetCashOutMoreThanHalfOfBalanceCondition();

            Assert.AreEqual(expectedResult, testObject.IsCashOutMoreThanHalfOfBalance);
        }
    }
}
