﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace ClassSuperTechChallenge.Provider
{
    /// <summary>
    /// Provider class to load customers from CSV file and cache the transaction data. 
    /// Generally the customers data should be sync with database.
    /// </summary>
    public class CustomersProvider : ICustomersProvider
    {
        public CustomersProvider()
        {
            Customers = new List<ICustomer>();
        }

        public IList<ICustomer> Customers { get; }

        /// <summary>
        /// Load customers from CSV file.
        /// </summary>
        /// <param name="path">CSV file path.</param>
        public void Load(string path)
        {
            var customers = CSVReader.ReadAs<ICustomer>(path, CreateCustomer, true);

            // Add customer to the list if it has not been added before
            foreach (var customer in customers)
            {
                if (!Customers.Any(c => c.Equals(customer)))
                {
                    Customers.Add(customer);
                }
            }
        }

        private Customer CreateCustomer(string[] fields)
        {
            if (fields.Length != 3)
            {
                return null;
            }

            if (!uint.TryParse(fields[0], out uint id))
            {
                return null;
            }

            return new Customer(id, fields[1], fields[2]);
        }
    }
}
