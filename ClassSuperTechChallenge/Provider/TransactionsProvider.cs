﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ClassSuperTechChallenge.Provider
{
    /// <summary>
    /// Provider class to load transactions from CSV file and cache the transaction data.
    /// Generally the transactions data should be sync with database.
    ///     
    /// It is better to use Service Provider to register this class with Singleton lifestyle, 
    /// but for simplicity I just make this class as singleton.
    /// </summary>
    public class TransactionsProvider : ITransactionsProvider
    {
        private IDictionary<uint, IList<ITransaction>> mGroupedTransactions;

        public TransactionsProvider()
        {
            Transactions = new List<ITransaction>();
            mGroupedTransactions = new Dictionary<uint, IList<ITransaction>>();
        }

        public IList<ITransaction> Transactions { get; }

        /// <summary>
        /// Load transactions from CSV file.
        /// </summary>
        /// <param name="path">CSV file path.</param>
        public void Load(string path)
        {
            var transactions = CSVReader.ReadAs<ITransaction>(path, CreateTransaction, true);

            // Add customer to the list if it has not been added before
            foreach (var transaction in transactions)
            {
                if (!mGroupedTransactions.TryGetValue(transaction.CustomerId, out IList<ITransaction> transactionGroup))
                {
                    transactionGroup = new List<ITransaction>();
                    mGroupedTransactions.Add(transaction.CustomerId, transactionGroup);
                }

                transactionGroup.Add(transaction);
                Transactions.Add(transaction);
            }
        }

        public IList<ITransaction> GetTransactions(uint customerId)
        {
            if(!mGroupedTransactions.TryGetValue(customerId, out IList<ITransaction> transactions))
            {
                transactions = new List<ITransaction>();
            }

            return transactions;
        }

        private Transaction CreateTransaction(string[] fields)
        {
            if (fields.Length != 6)
            {
                return null;
            }

            if (!uint.TryParse(fields[0], out uint customerId))
            {
                return null;
            }

            if (!uint.TryParse(fields[1], out uint accountId))
            {
                return null;
            }

            if (!decimal.TryParse(fields[2], out decimal amount))
            {
                return null;
            }

            if (!Enum.TryParse<TransactionCode>(fields[3], out TransactionCode transactionCode))
            {
                return null;
            }

            if (!decimal.TryParse(fields[4], out decimal balance))
            {
                return null;
            }

            if (!DateTime.TryParseExact(fields[5], "M/d/yyyy H:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
            {
                return null;
            }

            var transaction = new Transaction(customerId, accountId, amount, transactionCode, balance, date);

            return transaction;
        }
    }
}
