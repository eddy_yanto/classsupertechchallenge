﻿using ClassSuperTechChallenge.Model;
using System.Collections.Generic;

namespace ClassSuperTechChallenge.Provider
{
    public interface ICustomersProvider
    {
        /// <summary>
        /// List of customers.
        /// </summary>
        IList<ICustomer> Customers { get; }

        /// <summary>
        /// Load customers from a file.
        /// </summary>
        /// <param name="path">File path.</param>
        void Load(string path);
    }
}
