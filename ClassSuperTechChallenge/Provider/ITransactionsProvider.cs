﻿using ClassSuperTechChallenge.Model;
using System.Collections.Generic;

namespace ClassSuperTechChallenge.Provider
{
    public interface ITransactionsProvider
    {
        /// <summary>
        /// List of transactions.
        /// </summary>
        IList<ITransaction> Transactions { get; }

        /// <summary>
        /// Load transactions from a file.
        /// </summary>
        /// <param name="path">File path.</param>
        void Load(string path);

        /// <summary>
        /// Get transactions based on customer id.
        /// </summary>
        /// <param name="customerId">Customer Id.</param>
        /// <returns></returns>
        IList<ITransaction> GetTransactions(uint customerId);
    }
}
