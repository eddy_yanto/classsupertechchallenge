﻿using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.Utilities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace ClassSuperTechChallenge.ViewModel
{
    /// <summary>
    /// View model for Customers user control, contains list of customers.
    /// </summary>
    public class CustomersUserControlVM : INotifyPropertyChanged
    {
        private readonly ICustomersProvider mCustomersProvider;
        private readonly ITransactionsProvider mTransactionsProvider;

        public CustomersUserControlVM(ICustomersProvider customersProvider, ITransactionsProvider transactionsProvider)
        {
            Customers = new ObservableCollection<CustomerRowVM>();
            CustomersView = CollectionViewSource.GetDefaultView(Customers);
            mCustomersProvider = customersProvider;
            mTransactionsProvider = transactionsProvider;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<CustomerRowVM> Customers { get; }

        public ICollectionView CustomersView { get; }

        private CustomerRowVM mSelectedCustomer;
        public CustomerRowVM SelectedCustomer
        {
            get
            {
                return mSelectedCustomer;
            }

            set
            {
                mSelectedCustomer = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedCustomer)));

            }
        }

        private ICommand mLoadCustomersCommand;
        /// <summary>
        /// Command to load customers data.
        /// </summary>
        public ICommand LoadCustomersCommand
        {
            get
            {
                if (mLoadCustomersCommand == null)
                {
                    mLoadCustomersCommand = new RelayCommand(action => LoadCustomers());
                }

                return mLoadCustomersCommand;
            }
        }

        private ICommand mShowAllCustomersCommand;
        /// <summary>
        /// Command to list all customers.
        /// </summary>
        public ICommand ShowAllCustomersCommand
        {
            get
            {
                if (mShowAllCustomersCommand == null)
                {
                    mShowAllCustomersCommand = new RelayCommand(action => ShowAllCustomers());
                }

                return mShowAllCustomersCommand;
            }
        }

        private ICommand mShowCustomersWithMultipleAccountCommand;
        /// <summary>
        /// Command to list customers with multiple accounts.
        /// </summary>
        public ICommand ShowCustomersWithMultipleAccountCommand
        {
            get
            {
                if (mShowCustomersWithMultipleAccountCommand == null)
                {
                    mShowCustomersWithMultipleAccountCommand = new RelayCommand(action => FilterCustomersWithMultipleAccount());
                }

                return mShowCustomersWithMultipleAccountCommand;
            }
        }

        private ICommand mShowCustomersWithoutAccountCommand;
        /// <summary>
        /// Command to list customers without any account.
        /// </summary>
        public ICommand ShowCustomersWithoutAccountCommand
        {
            get
            {
                if (mShowCustomersWithoutAccountCommand == null)
                {
                    mShowCustomersWithoutAccountCommand = new RelayCommand(action => FilterCustomersWithoutAccount());
                }

                return mShowCustomersWithoutAccountCommand;
            }
        }

        private void LoadCustomers()
        {
            var groupedTransaction = mTransactionsProvider.Transactions;

            foreach (var customer in mCustomersProvider.Customers)
            {
                Customers.Add(new CustomerRowVM(customer, mTransactionsProvider));
            }
        }

        private void ShowAllCustomers()
        {
            CustomersView.Filter = null;
        }

        private void FilterCustomersWithMultipleAccount()
        {
            CustomersView.Filter = item =>
            {
                var customer = item as CustomerRowVM;
                return customer.NoOfAccount > 1;
            };
        }

        private void FilterCustomersWithoutAccount()
        {
            CustomersView.Filter = item =>
            {
                var customer = item as CustomerRowVM;
                return customer.NoOfAccount == 0;
            };
        }
    }
}
