﻿using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.Utilities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace ClassSuperTechChallenge.ViewModel
{
    /// <summary>
    /// View model for Transaction user control, contains list of transactions for specific customer.
    /// </summary>
    public class TransactionsUserControlVM
    {
        private readonly ITransactionsProvider mTransactionsProvider;

        public TransactionsUserControlVM(ITransactionsProvider transactionsProvider)
        {
            Transactions = new ObservableCollection<TransactionRowVM>();
            TransactionsView = CollectionViewSource.GetDefaultView(TransactionsView);
            mTransactionsProvider = transactionsProvider;
        }

        public ObservableCollection<TransactionRowVM> Transactions { get; }

        public ICollectionView TransactionsView { get; }

        private ICommand mResetConditionsCommand;
        /// <summary>
        /// Command to reset all filtering conditions.
        /// </summary>
        public ICommand ResetConditionsCommand
        {
            get
            {
                if (mResetConditionsCommand == null)
                {
                    mResetConditionsCommand = new RelayCommand(action => ResetConditions());
                }

                return mResetConditionsCommand;
            }
        }

        private ICommand mIdentifyAfterHoursTransactionsCommand;
        /// <summary>
        /// Command to set condition for transactions between 18.00 and 06.00.
        /// </summary>
        public ICommand IdentifyAfterHoursTransactionsCommand
        {
            get
            {
                if (mIdentifyAfterHoursTransactionsCommand == null)
                {
                    mIdentifyAfterHoursTransactionsCommand = new RelayCommand(action => IdentifyAfterHoursTransactions());
                }

                return mIdentifyAfterHoursTransactionsCommand;
            }
        }

        private ICommand mIdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand;
        /// <summary>
        /// Command to set condition for cash out transaction more than half of balance.
        /// </summary>
        public ICommand IdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand
        {
            get
            {
                if (mIdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand == null)
                {
                    mIdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand = new RelayCommand(action => IdentifyCashOutMoreThanHalfOfBalanceTransactions());
                }

                return mIdentifyCashOutMoreThanHalfOfBalanceTransactionsCommand;
            }
        }

        /// <summary>
        /// Load transactions based on customer.
        /// </summary>
        /// <param name="customerId"></param>
        public void LoadTransactions(CustomerRowVM customer)
        {
            Transactions.Clear();

            if (customer != null)
            {
                var transactions = mTransactionsProvider.GetTransactions(customer.CustomerId);
                foreach (var transaction in transactions)
                {
                    Transactions.Add(new TransactionRowVM(transaction));
                }
            }
        }

        private void ResetConditions()
        {
            foreach (var transaction in Transactions)
            {
                transaction.ClearConditions();
            }
        }

        private void IdentifyAfterHoursTransactions()
        {
            foreach (var transaction in Transactions)
            {
                transaction.ClearConditions();
                transaction.SetAfterHoursCondition();
            }
        }

        private void IdentifyCashOutMoreThanHalfOfBalanceTransactions()
        {
            foreach (var transaction in Transactions)
            {
                transaction.ClearConditions();
                transaction.SetCashOutMoreThanHalfOfBalanceCondition();
            }
        }
    }
}
