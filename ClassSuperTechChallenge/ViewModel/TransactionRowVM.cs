﻿using ClassSuperTechChallenge.Model;
using System;
using System.ComponentModel;

namespace ClassSuperTechChallenge.ViewModel
{
    /// <summary>
    /// View model for Transaction row
    /// </summary>
    public class TransactionRowVM : INotifyPropertyChanged
    {
        private readonly ITransaction mTransaction;

        public event PropertyChangedEventHandler PropertyChanged;

        public TransactionRowVM(ITransaction transaction)
        {
            mTransaction = transaction;
        }

        public uint AccountId => mTransaction.AccountId;

        public decimal Amount => mTransaction.Amount;

        public TransactionCode TransactionCode => mTransaction.TransactionCode;

        public decimal Balance => mTransaction.Balance;

        public DateTime Date => mTransaction.Date;

        private bool mIsAfterHoursTransaction;

        /// <summary>
        /// Flag is set to true if transaction time between 18.00 and 06.00 by calling <see cref="SetAfterHoursCondition"/>
        /// </summary>
        public bool IsAfterHoursTransaction
        {
            get
            {
                return mIsAfterHoursTransaction;
            }

            private set
            {
                mIsAfterHoursTransaction = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsAfterHoursTransaction)));
            }
        }

        private bool mIsCashOutMoreThanHalfOfBalance;

        /// <summary>
        /// Flag is set to true if transaction is cash out and more than half of balance amount by calling <see cref="SetCashOutMoreThanHalfOfBalanceCondition"/>
        /// </summary>
        public bool IsCashOutMoreThanHalfOfBalance
        {
            get
            {
                return mIsCashOutMoreThanHalfOfBalance;
            }

            private set
            {
                mIsCashOutMoreThanHalfOfBalance = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCashOutMoreThanHalfOfBalance)));
            }
        }

        /// <summary>
        /// Reset all conditions, i.e. <see cref="IsAfterHoursTransaction"/> and <see cref="IsCashOutMoreThanHalfOfBalance"/>
        /// </summary>
        public void ClearConditions()
        {
            IsAfterHoursTransaction = false;
            IsCashOutMoreThanHalfOfBalance = false;
        }

        /// <summary>
        /// Set <see cref="IsAfterHoursTransaction"/> flag to true if transaction time between 18.00 and 06.00
        /// </summary>
        public void SetAfterHoursCondition()
        {
            if (Date.TimeOfDay >= new TimeSpan(18, 0, 0) || Date.TimeOfDay <= new TimeSpan(6, 0, 0))
            {
                IsAfterHoursTransaction = true;
            }
        }

        /// <summary>
        /// Set <see cref="IsCashOutMoreThanHalfOfBalance"/> flag to true if transaction is cash out and more than half of balance amount
        /// </summary>
        public void SetCashOutMoreThanHalfOfBalanceCondition()
        {
            if (TransactionCode == TransactionCode.COT && Amount > (Amount + Balance) / 2)
            {
                IsCashOutMoreThanHalfOfBalance = true;
            }
        }
    }
}
