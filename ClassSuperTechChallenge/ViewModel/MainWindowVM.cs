﻿using System.ComponentModel;

namespace ClassSuperTechChallenge.ViewModel
{
    public class MainWindowVM
    {
        public MainWindowVM(CustomersUserControlVM customersUserControlVM, TransactionsUserControlVM transactionsUserControlVM)
        {
            CustomersUserControlVM = customersUserControlVM;
            TransactionsUserControlVM = transactionsUserControlVM;

            CustomersUserControlVM.PropertyChanged += CustomersUserControlVM_PropertyChanged;
        }

        /// <summary>
        /// View model for Customers user control
        /// </summary>
        public CustomersUserControlVM CustomersUserControlVM { get; }

        /// <summary>
        /// View model for Transactions user control
        /// </summary>
        public TransactionsUserControlVM TransactionsUserControlVM { get; }

        private void CustomersUserControlVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(CustomersUserControlVM.SelectedCustomer)))
            {
                TransactionsUserControlVM.LoadTransactions(CustomersUserControlVM.SelectedCustomer);
            }
        }

    }
}
