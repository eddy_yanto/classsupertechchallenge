﻿using ClassSuperTechChallenge.Model;
using ClassSuperTechChallenge.Provider;
using System.Collections.Generic;
using System.Linq;

namespace ClassSuperTechChallenge.ViewModel
{
    /// <summary>
    /// View model for Customer row
    /// </summary>
    public class CustomerRowVM
    {
        private readonly ICustomer mCustomer;
        private readonly ITransactionsProvider mTransactionsProvider;
        private readonly IList<ITransaction> mTransactions;
        private readonly int mNoOfAccount;
        private readonly decimal mBalance;

        public CustomerRowVM(ICustomer customer, ITransactionsProvider transactionsProvider)
        {
            mCustomer = customer;
            mTransactionsProvider = transactionsProvider;
            mTransactions = transactionsProvider.GetTransactions(customer.Id);

            var groupedTransactions = mTransactions.GroupBy(t => t.AccountId).ToList();
            mNoOfAccount = groupedTransactions.Count;
            mBalance = groupedTransactions.Sum(g => g.Last().Balance);
        }

        public uint CustomerId => mCustomer.Id;

        public string FirstName => mCustomer.FirstName;

        public string LastName => mCustomer.LastName;

        public int NoOfAccount => mNoOfAccount;

        public decimal Balance => mBalance;
    }
}
