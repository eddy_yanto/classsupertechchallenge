﻿using ClassSuperTechChallenge.Provider;
using ClassSuperTechChallenge.View;
using ClassSuperTechChallenge.ViewModel;
using SimpleInjector;
using System;

namespace ClassSuperTechChallenge
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var container = Bootstrap();

            RunApplication(container);
        }

        private static Container Bootstrap()
        {
            // Create the container as usual.
            var container = new Container();

            // Register all providers
            container.RegisterSingleton<ICustomersProvider, CustomersProvider>();
            container.RegisterSingleton<ITransactionsProvider, TransactionsProvider>();

            // Register windows and view models
            container.RegisterSingleton<MainWindow>();
            container.RegisterSingleton<MainWindowVM>();
            container.RegisterSingleton<CustomersUserControl>();
            container.RegisterSingleton<CustomersUserControlVM>();
            container.RegisterSingleton<TransactionsUserControl>();
            container.RegisterSingleton<TransactionsUserControlVM>();

            container.Verify();

            return container;
        }

        private static void RunApplication(Container container)
        {
            // For simplicity we use constant path to load the files.
            container.GetInstance<ICustomersProvider>().Load("Resources/Customers.csv");
            container.GetInstance<ITransactionsProvider>().Load("Resources/Transactions.csv");

            var app = new App();
            var mainWindow = container.GetInstance<MainWindow>();
            mainWindow.DataContext = container.GetInstance<MainWindowVM>();
            app.Run(mainWindow);
        }
    }
}
