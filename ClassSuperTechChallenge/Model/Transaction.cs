﻿using System;
using System.Diagnostics;

namespace ClassSuperTechChallenge.Model
{
    /// <summary>
    /// Concrete class implemetation of transaction models.
    /// </summary>
    [DebuggerDisplay("{CustomerId}|{AccountId}|{Amount}|{TransactionCode}|{Balance}|{Date}")]
    public class Transaction : ITransaction
    {
        public Transaction(uint customerId, uint accountId, decimal amount, TransactionCode transactionCode, decimal balance, DateTime date)
        {
            CustomerId = customerId;
            AccountId = accountId;
            Amount = amount;
            TransactionCode = transactionCode;
            Balance = balance;
            Date = date;
        }

        public uint CustomerId { get; }

        public uint AccountId { get; }

        public decimal Amount { get; }

        public TransactionCode TransactionCode { get; }

        public decimal Balance { get; }

        public DateTime Date { get; }
    }
}
