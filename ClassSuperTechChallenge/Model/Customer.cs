﻿namespace ClassSuperTechChallenge.Model
{
    /// <summary>
    /// Concrete cslass implementation of Customer model.
    /// </summary>
    public class Customer : ICustomer
    { 
        public Customer(uint id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }

        public uint Id { get; }

        public string FirstName { get; }

        public string LastName { get; }
    }
}
