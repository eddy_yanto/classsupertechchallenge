﻿namespace ClassSuperTechChallenge.Model
{
    public enum TransactionCode
    {
        /// <summary>
        /// Cash In
        /// </summary>
        CIN,

        /// <summary>
        /// Cash Out
        /// </summary>
        COT,
    }
}
