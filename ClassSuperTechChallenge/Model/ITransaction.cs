﻿using System;

namespace ClassSuperTechChallenge.Model
{
    /// <summary>
    /// Generic interface for transaction containing Customer Id, Account Id and other transaction data.
    /// </summary>
    public interface ITransaction
    {
        /// <summary>
        /// Customer identifier
        /// </summary>
        uint CustomerId { get; }

        /// <summary>
        /// Account Identifier
        /// </summary>
        uint AccountId { get; }

        /// <summary>
        /// Amount of T=transaction 
        /// </summary>
        decimal Amount { get; }

        /// <summary>
        /// Transaction code
        /// </summary>
        TransactionCode TransactionCode { get; }

        /// <summary>
        /// Account Balance after the transaction
        /// </summary>
        decimal Balance { get; }

        /// <summary>
        /// Date of transaction
        /// </summary>
        DateTime Date { get; }
    }
}
