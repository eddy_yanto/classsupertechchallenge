﻿namespace ClassSuperTechChallenge.Model
{
    /// <summary>
    /// Generic Interface for customer, containing Id, First Name and Last Name.
    /// </summary>
    public interface ICustomer
    {
        /// <summary>
        /// Customer Id.
        /// </summary>
        uint Id { get; }

        /// <summary>
        /// Customer First Name
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Customer Last Names
        /// </summary>
        string LastName { get; }
    }
}
