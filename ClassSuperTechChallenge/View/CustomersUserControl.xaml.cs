﻿using System.Windows.Controls;

namespace ClassSuperTechChallenge.View
{
    /// <summary>
    /// Interaction logic for CustomersUserControl.xaml
    /// </summary>
    public partial class CustomersUserControl : UserControl
    {
        public CustomersUserControl()
        {
            InitializeComponent();
        }
    }
}
