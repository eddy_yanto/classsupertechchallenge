﻿using System.Windows.Controls;

namespace ClassSuperTechChallenge.View
{
    /// <summary>
    /// Interaction logic for TransactionsUserControl.xaml
    /// </summary>
    public partial class TransactionsUserControl : UserControl
    {
        public TransactionsUserControl()
        {
            InitializeComponent();
        }
    }
}
