﻿using System;
using System.Windows.Input;

namespace ClassSuperTechChallenge.Utilities
{
    /// <summary>
    /// Simple implementation of ICommand to relay the action logic.
    /// This is added to simplify ICommand implementation.
    /// </summary>
    public class RelayCommand : ICommand
    {
        readonly Action<object> mExecute;
        readonly Predicate<object> mCanExecute;

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute) : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            mExecute = execute;
            mCanExecute = canExecute;
        }

        public bool CanExecute(object parameters)
        {
            return mCanExecute == null ? true : mCanExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            mExecute(parameters);
        }
    }
}
