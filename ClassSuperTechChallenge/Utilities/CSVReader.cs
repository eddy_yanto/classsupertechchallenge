﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;

namespace ClassSuperTechChallenge.Utilities
{
    public class CSVReader
    {
        /// <summary>
        /// Read CSV file and convert it to collection of <paramref name="T"/>.
        /// </summary>
        /// <typeparam name="T">Converted object.</typeparam>
        /// <param name="filePath">Path of csv file.</param>
        /// <param name="converterFunc">Mapping function to map fields to <paramref name="T"/>.</param>
        /// <param name="skipHeader">flag to skip header or not.</param>
        /// <returns></returns>
        public static T[] ReadAs<T>(string filePath, Func<string[], T> converterFunc, bool skipHeader)
        {
            using (var csvParser = new TextFieldParser(filePath))
            {
                csvParser.SetDelimiters(new string[] { "," });

                // Skip first line
                if (skipHeader)
                {
                    csvParser.ReadLine();
                }

                // Read line by line and convert to collection of T
                var objectList = new List<T>();
                while (!csvParser.EndOfData)
                {
                    var fields = csvParser.ReadFields();
                    var convertedObject = converterFunc(fields);
                    if (convertedObject != null)
                    {
                        objectList.Add(convertedObject);
                    }
                }

                return objectList.ToArray();
            }
        }
    }
}
